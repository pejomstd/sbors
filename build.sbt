name := "Scala-Bors"

version := "0.5"

organization := "org.ironic"

scalaVersion := "2.11.6"

//resolvers += {
//  val typesafeRepoUrl = new java.net.URL("http://repo.typesafe.com/typesafe/releases")
//  val pattern = Patterns(false, "[organisation]/[module]/[sbtversion]/[revision]/[type]s/[module](-[classifier])-[revision].[ext]")
//  Resolver.url("Typesafe Repository", typesafeRepoUrl)(pattern)
//}

resolvers += "Scala-Tools Maven2 Snapshots Repository" at "http://scala-tools.org/repo-snapshots"

//libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.3"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"

libraryDependencies += "com.typesafe" % "config" % "1.2.0"

libraryDependencies ++= Seq(
    "org.scalacheck" %% "scalacheck" % "1.12.3",
    "org.scalafx" %% "scalafx" % "8.0.0-R4"
)

// Add dependency on JavaFX library based on JAVA_HOME variable
// unmanagedJars in Compile += Attributed.blank(file(System.getenv("JAVA_HOME") + "/jre/lib/jfxrt.jar"))
