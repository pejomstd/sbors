package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class CoppockStage extends LineGraphStage {
  title = "Coppock"
  
  val closes = ScalaBorsApp.curPriceList.map { p => p.last }.toList
  val model = CoppockUtil.coppock(closes, 14, 11, 10)
  val avg = MAvgUtil.simpleMovingAvg(model, 10)
  
  val modelData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, model(i))
  val avgData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, avg(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(modelData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val data2 = ObservableBuffer(avgData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val series1 = XYChart.Series[String, Number]("Coppock", data1)
  val series2 = XYChart.Series[String, Number]("MAvg", data2)
  lineChart.getData().add(series1)
  lineChart.getData().add(series2)
  lineChart.stylesheets += this.getClass.getResource("/CoppockStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "Coppock"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object CoppockStage {
  def apply() = new CoppockStage()
}