package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.control.{Label, TextField, RadioButton, ComboBox, Button}
import scalafx.scene.layout._
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.geometry.{Insets, Pos, Orientation}
import scalafx.event.ActionEvent
import javafx.beans.value.{ChangeListener,ObservableValue}

import org.ironic.sbors.service.PriceService
import org.ironic.sbors.util.PriceLoad

class PriceLoadStage extends Stage {
  title = "Settings"
  width = 600
  height = 400
  
  val opts = PriceService.getPriceLists.map { sl => sl.id + ": " + sl.name}
  var shareList: String = ""
  
  val instrCombo = new ComboBox[String](opts) {
    value.addListener(new ChangeListener[String]() {
        @Override 
        def changed(ov: ObservableValue[_ <: String],t: String,t1: String) = {                
          println("Choosen... " + t1) 
          shareList = t1.split(":")(0)
        }    
      });
  }
    
  scene = new Scene {
    import scalafx.scene.paint.Color

    fill = Color.LIGHTGRAY
    root = new FlowPane() {
      padding = Insets(10)
      content = Seq(
        instrCombo,
        new Button("Import") {
          onAction =  (ae: ActionEvent) => doImport
        }
      )
    }
  }
  
  private def doImport() = {
    if(!shareList.isEmpty) {
      PriceLoad(shareList).importShares
    }
  }
}

object PriceLoadStage {
  def apply() = new PriceLoadStage()
}
