package org.ironic.sbors.view

import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}

class LineGraphStage  extends Stage {
//  title = "Stochastics"
  width = 1000
  height = 280
  val xAxis = new CategoryAxis();
  val yAxis = new NumberAxis();
  xAxis.setLabel("Date");
  val lineChart = LineChart(xAxis,yAxis) 
  lineChart.prefWidth = 2000
  lineChart.prefHeight = 250
  
}
