package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class StochasticsStage extends Stage{
  width = 1000
  height = 280
  val xAxis = new CategoryAxis();
  val yAxis = NumberAxis(0,100,20);
  xAxis.setLabel("Date");
  val lineChart = LineChart(xAxis,yAxis) 
  lineChart.prefWidth = 2000
  lineChart.prefHeight = 250
  
  title = "Stochastics"
  lineChart.title = "Stochastics"

  println("Fast st." + Settings.stochasticFast)
  val sData = PriceUtil.stochastics(ScalaBorsApp.curPriceList, Settings.stochasticK)
  val stoch = if(Settings.stochasticFast) {
    sData
  } else {
    MAvgUtil.simpleMovingAvg(sData, Settings.stochasticSmooth)
  }
  val slidingStoch = MAvgUtil.simpleMovingAvg(stoch, Settings.stochasticD)

  val stoData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, stoch(i), slidingStoch(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(stoData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val data2 = ObservableBuffer(stoData) map {t => XYChart.Data[String, Number](t._1, t._3).delegate}
  val series1 = XYChart.Series[String, Number]("Stoch", data1)
  val series2 = XYChart.Series[String, Number]("StochAvg", data2)
  lineChart.getData().add(series1)
  lineChart.getData().add(series2)
  lineChart.stylesheets += this.getClass.getResource("/StochasticStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "Stochastics"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object StochasticsStage {
  def apply() = new StochasticsStage()
}