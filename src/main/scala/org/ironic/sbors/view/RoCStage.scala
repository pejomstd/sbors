package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class RoCStage extends LineGraphStage {
  title = "RoC"
  
  val closes = ScalaBorsApp.curPriceList.map{p => p.last}.toList
  val roc = PriceUtil.roc(closes,10)
  
  val rocData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, roc(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(rocData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val series1 = XYChart.Series[String, Number]("RoC", data1)
  lineChart.getData().add(series1)
  lineChart.stylesheets += this.getClass.getResource("/RSIStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "RoC"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object RoCStage {
  def apply() = new RoCStage()
}