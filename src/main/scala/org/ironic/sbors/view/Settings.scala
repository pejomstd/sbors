package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.control.{Label, TextField, RadioButton, ToggleGroup, Button}
import scalafx.scene.layout._
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.event.ActionEvent
import scalafx.geometry.{Insets, Pos, Orientation}

import com.typesafe.config._

class SettingsStage extends Stage {
  title = "Settings"
  width = 600
  height = 400
  val txtFromDate = new TextField {
    text = Settings.chartFrom
  }
  val txtStochPer = new TextField {
    text = Settings.stochasticK.toString
  }
  val txtStochSignPer = new TextField{
    text = Settings.stochasticD.toString
  }
    
  val txtRsiPer = new TextField{
    text = Settings.rsiPeriods.toString
  }
    
  val txtMavgA = new TextField{
    text = Settings.mAvgP1.toString
  }
    
  val txtMavgB = new TextField{
    text = Settings.mAvgP2.toString
  }
    
  val txtMavgC = new TextField{
    text = Settings.mAvgP3.toString
  }
  
  val rbGroup = new ToggleGroup {}
  
  val rbSMA = new RadioButton {
    text = "SMA"
    toggleGroup = rbGroup
  }
  val rbEMA = new RadioButton {
    text = "EMA"
    toggleGroup = rbGroup
  }
  
  val selected = if (Settings.isEMA) rbEMA else rbSMA
  rbGroup.selectToggle(selected)
  
  val txtAroonPer = new TextField{
    text = Settings.aroonPeriods.toString
  }
    
  val txtMacdPerA = new TextField{
    text = Settings.macdPeriodsA.toString
  }
    
  val txtMacdPerB = new TextField{
    text = Settings.macdPeriodsB.toString
  }
    
  val txtMacdSignPer = new TextField {
    text = Settings.macdSignalPeriods.toString
  }
  
  val txtCoppockRoC1 = new TextField {
    text = Settings.coppockRoC1.toString
  }
  
  val txtCoppockRoC2 = new TextField {
    text = Settings.coppockRoC2.toString
  }
  
  val txtCoppockWMA = new TextField {
    text = Settings.coppockWMA.toString
  }
  
  val txtCoppockSignal = new TextField {
    text = Settings.coppockSignal.toString
  }
  
  def applySettings() : Unit = {
    println("Apply settings: ")
    Settings.chartFrom = txtFromDate.text.get
    Settings.stochasticK = txtStochPer.text.get.toInt
    Settings.stochasticD = txtStochSignPer.text.get.toInt
    Settings.rsiPeriods = txtRsiPer.text.get.toInt
    Settings.aroonPeriods = txtAroonPer.text.get.toInt
    Settings.macdPeriodsA = txtMacdPerA.text.get.toInt
    Settings.macdPeriodsB = txtMacdPerB.text.get.toInt
    Settings.macdSignalPeriods = txtMacdSignPer.text.get.toInt
      
    Settings.isEMA = rbEMA.isSelected
    
    Settings.mAvgP1 = txtMavgA.text.get.toInt
    Settings.mAvgP2 = txtMavgB.text.get.toInt
    Settings.mAvgP3 = txtMavgC.text.get.toInt
    
    Settings.coppockRoC1 = txtCoppockRoC1.text.get.toInt
    Settings.coppockRoC2 = txtCoppockRoC2.text.get.toInt
    Settings.coppockWMA = txtCoppockWMA.text.get.toInt
    Settings.coppockSignal = txtCoppockSignal.text.get.toInt
  }
  
  val btnApply = new Button {
    text = "Apply"
    onAction = (ae: ActionEvent) => applySettings()
    
//        onAction    applyChanges()
  }
  
  
  
  scene = new Scene {
    import scalafx.scene.paint.Color

    fill = Color.LIGHTGRAY
    root = new GridPane() {
      padding = Insets(10)
      add(new Label("From date :"),0,0)
      add(txtFromDate,1,0)
      add(new Label("Stochastics per:"),0,1)
      add(txtStochPer,1,1)
      
      add(new Label("Stochastics sign:"), 2, 1) 
      add(txtStochSignPer, 3, 1)
      add(new Label("RSI per:"), 0, 2)
      add(txtRsiPer, 1, 2)
      add(new Label("Moving avg.:"), 0, 3)
      add(txtMavgA, 1, 3)
      add(txtMavgB, 2, 3)
      add(txtMavgC, 3, 3)
      add(rbSMA, 1, 4)
      add(rbEMA, 2, 4)
      add(new Label("Aroon per:"), 0, 5)
      add(txtAroonPer, 1, 5)
      
      add(new Label("MACD (p1,p2,sign):"), 0, 6)
      add(txtMacdPerA, 1, 6)
      add(txtMacdPerB, 2, 6)
      add(txtMacdSignPer, 3, 6)
      add(new Label("Coppock (r1,r2,wma,sign):"), 0, 7)
      add(txtCoppockRoC1, 1, 7)
      add(txtCoppockRoC2, 2, 7)
      add(txtCoppockWMA, 3, 7)
      add(txtCoppockSignal, 4, 7)
      add(btnApply, 3, 8)
    
    }
  }
//  style = "-fx-background-color: #336699;"
}

object SettingsStage  {
//  var fromDate = "201306"
//  
//  var stochasticPeriods = 14
//  var stochasticSignalPeriods = 3
//  var rsiPeriods = 14
//  var movingAvgPeriods = List(10,20,50)
//  
//  var aroonPeriods = 25
//  
//  var macdPeriodsA = 12
//  var macdPeriodsB = 26
//  var macdSignalPeriods = 9
//  var isEMA = true
//  
  def apply() = {
    new SettingsStage()
  }
}


object Settings {
  val cf = new java.io.File("conf/sbors.conf")
//  val config = ConfigFactory.load("sbors").getConfig("sbors")
  val config = ConfigFactory.parseFile(cf).getConfig("sbors")
  
  var chartFrom = config.getString("chart.from")
  
  var stochasticK = config.getInt("stochastics.periods.k")
  var stochasticD = config.getInt("stochastics.periods.d")
  var stochasticSmooth = config.getInt("stochastics.periods.s")
  var stochasticFast = config.getBoolean("stochastics.fast")
  var rsiPeriods = config.getInt("rsi.period")
  var movingAvgPeriods = List(10,20,50)
  var mAvgP1 = config.getInt("moving-average.periods.1")
  var mAvgP2 = config.getInt("moving-average.periods.2")
  var mAvgP3 = config.getInt("moving-average.periods.3")
  
  var aroonPeriods = config.getInt("aroon.period")
  
  var macdPeriodsA = config.getInt("macd.avg.1")
  var macdPeriodsB = config.getInt("macd.avg.2")
  var macdSignalPeriods = config.getInt("macd.signal")
  var isEMA = config.getString("moving-average.type")=="EMA"
  
  var coppockRoC1 = config.getInt("coppock.roc.1")
  var coppockRoC2 = config.getInt("coppock.roc.2")
  var coppockWMA = config.getInt("coppock.wma")
  var coppockSignal = config.getInt("coppock.signal")
}
