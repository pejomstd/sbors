package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class RSIStage extends LineGraphStage {
  title = "RSI"
  
  val rsi = PriceUtil.RSI(PriceUtil.closesList(ScalaBorsApp.curPriceList),Settings.rsiPeriods)
  
  val rsiData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, rsi(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(rsiData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val series1 = XYChart.Series[String, Number]("RSI", data1)
  lineChart.getData().add(series1)
  lineChart.stylesheets += this.getClass.getResource("/RSIStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "RSI"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object RSIStage {
  def apply() = new RSIStage()
}