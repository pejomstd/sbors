package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class AroonStage extends LineGraphStage {
  title = "Aroon"
  
  val aroon1 = AroonUtil.aroonList(ScalaBorsApp.curPriceList, Settings.aroonPeriods, true)
  val aroon2 = AroonUtil.aroonList(ScalaBorsApp.curPriceList, Settings.aroonPeriods, false)
  
  val aroonData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } yield (ScalaBorsApp.curPriceList(i).date, aroon1(i), aroon2(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(aroonData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val data2 = ObservableBuffer(aroonData) map {t => XYChart.Data[String, Number](t._1, t._3).delegate}
  val series1 = XYChart.Series[String, Number]("Aroon-1", data1)
  val series2 = XYChart.Series[String, Number]("Aroon-1", data2)
  lineChart.getData().add(series1)
  lineChart.getData().add(series2)
  lineChart.stylesheets += this.getClass.getResource("/AroonStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "Aroon"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object AroonStage {
  def apply() = new AroonStage()
}