package org.ironic.sbors.view

import javafx.scene.{chart => jfxsc}

import scalafx.stage.Stage
import scalafx.scene.chart.{AreaChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.collections.ObservableBuffer


import org.ironic.sbors.model._
import org.ironic.sbors.util._

class ChaikinMFStage extends Stage {
  title = "Chaikin Money Flow"
  width = 1000
  height = 280
  val xAxis = new CategoryAxis();
  val yAxis = new NumberAxis();
  xAxis.setLabel("Date");
  val barChart = AreaChart(xAxis,yAxis) 
  barChart.prefWidth = 2000
  barChart.prefHeight = 250
  
  barChart.data = createChartData(ScalaBorsApp.curPriceList)
  barChart.stylesheets += this.getClass.getResource("/VolumeStyle.css").toExternalForm
  scene = new Scene { 
    title = "Chaikin MF"
    root =  new ScrollPane {
      content = barChart
    }
    
  }
  
  def createChartData(data: List[Price]) = {
    val series = new XYChart.Series[String, Number]
    val cmf = PriceUtil.chaikingMF(data, 20)
    
    for (i <- 0 until data.size) {
      series.data() += XYChart.Data[String, Number](data(i).date, cmf(i))
    }
    val answer = new ObservableBuffer[jfxsc.XYChart.Series[String, Number]]()
    answer += series
    answer
    
  }
}

object ChaikinMFStage {
  def apply() = new ChaikinMFStage
}