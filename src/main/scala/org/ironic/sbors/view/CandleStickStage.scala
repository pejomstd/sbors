package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.stage.Stage
import scalafx.scene.chart.{XYChart, Axis, NumberAxis, CategoryAxis}
import scalafx.scene.{Scene}
import scalafx.scene.control.ScrollPane
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.util._

class CandleStickStage extends Stage {
  width = 1200
  height = 700
  
  val avg = MAvgUtil.simpleMovingAvg(PriceUtil.closesList(ScalaBorsApp.curPriceList), Settings.movingAvgPeriods(0))
  
  val data = (
    for(i <- 0 until ScalaBorsApp.curPriceList.size) 
      yield ScalaBorsApp.curPriceList(i) match{ case p=> CandleStick(p.date, p.open, p.last, p.high, p.low, avg(i))}
  ).toList

  
  protected def createChart(data : List[CandleStick]): CandleStickChart = {
    //Style Sheet loaded from external
    val css = this.getClass.getResource("/CandleStickStyle.css").toExternalForm

//    val xAxis =  NumberAxis(0, data.size, 1)
    val xAxis =  new CategoryAxis()
    xAxis.setLabel("Day")
//    xAxis.minorTickCount = 0
    
    
    val max = PriceUtil.max(ScalaBorsApp.curPriceList)
    val min = PriceUtil.min(ScalaBorsApp.curPriceList)
    val yAxis = NumberAxis(min, max, (max-min)*0.1)
    yAxis.setLabel("Price")
    
    val seriesData = data.map {d => XYChart.Data[String, Number](d.day, d.open, d).delegate}

    val series = XYChart.Series[String, Number]("Candle stick", ObservableBuffer(seriesData.toSeq))

    val csc = new CandleStickChart(xAxis, yAxis) {
      title = "Candle Stick Chart"
      data = ObservableBuffer(series)
      getStylesheets += css
    }
    csc.setPrefWidth(2000)
    csc.setPrefHeight(700)
    csc
  }

  scene = new Scene { 
    title = "Candle Stick"
    root =  new ScrollPane {
      content = createChart(data)
    }


  }
}

object CandleStickStage {
  def apply() = new CandleStickStage()
}