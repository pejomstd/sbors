package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.scene.layout._
import scalafx.scene.control.ScrollPane
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.view._
import org.ironic.sbors.util._

class MACDStage extends LineGraphStage {
  title = "MACD"
  
  val closeList = ScalaBorsApp.curPriceList map{p => p.last}
  val macdLine = MAvgUtil.macd(closeList, Settings.macdPeriodsA, Settings.macdPeriodsB)
  val signalLine = MAvgUtil.expMovingAvg(macdLine, Settings.macdSignalPeriods)
  val macdHistogram = MAvgUtil.macdHistogram(macdLine, signalLine)
  
  val macdData = for{ i <- 0 until ScalaBorsApp.curPriceList.size } 
  yield (ScalaBorsApp.curPriceList(i).date, macdLine(i), signalLine(i), macdHistogram(i))
//  println(stoData)
//  val data = ObservableBuffer(ScalaBorsApp.curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
  val data1 = ObservableBuffer(macdData) map {t => XYChart.Data[String, Number](t._1, t._2).delegate}
  val data2 = ObservableBuffer(macdData) map {t => XYChart.Data[String, Number](t._1, t._3).delegate}
  val data3 = ObservableBuffer(macdData) map {t => XYChart.Data[String, Number](t._1, t._4).delegate}
  val series1 = XYChart.Series[String, Number]("MACD", data1)
  val series2 = XYChart.Series[String, Number]("MACD-signal", data2)
  val series3 = XYChart.Series[String, Number]("MACD-histogram", data3)
  lineChart.getData().add(series1)
  lineChart.getData().add(series2)
  lineChart.getData().add(series3)
  lineChart.stylesheets += this.getClass.getResource("/MACDStyle.css").toExternalForm
  
  scene = new Scene { 
    title = "MACD"
    root =  new ScrollPane {
      content = lineChart
    }
    
    
  }
}

object MACDStage {
  def apply() = new MACDStage()
}