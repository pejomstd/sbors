package org.ironic.sbors.service

//import org.ironic.sbors.util.Logging
object PriceService { // extends Logging {
  import org.ironic.sbors.model._
  
  val pricelists = {
//    info("# loading pricelists")
    println("# loading pricelists")
    //PriceDAO.loadPriceLists
    PriceDAO.loadShareLists
  }
  
  def searchSharesByTicker(ticker: String) : List[Share]= {
    val list = getPriceLists map { kv => kv.instruments } flatMap { x => x }
    list.filter {
      s: Share => s.ticker.toLowerCase().startsWith(ticker.toLowerCase)
    }
  }
  
//  def searchSharesByTicker(s: String) : List[Share]= {
//    getPriceLists map { kv => kv.instruments } flatMap { 
//      case share: Share => share 
//      case x => x
//    } filter {
//      i: Share => i.ticker.toLowerCase().startsWith(s.toLowerCase)
//    }
//  }
  
  def getPriceLists = {
     // using already loaded pricelists, i.e loading is done in val...
     pricelists
  }
  
  def getShareData(inst: Share) = {
    PriceDAO.loadShareData(inst)
  }
  
  def addToWatchList(watch: Share) = {
    // inst may come in without real name so get it first.
    val inst = searchSharesByTicker(watch.ticker).head
//    info("Adding to watch list : " + inst)
    println("Adding to watch list : " + inst)
    PriceDAO.addToWatchList(inst)
  }
  
  def getWatchList() : List[Share] = {
    PriceDAO.getWatchList()
  }
  
  def removeFromWatchList(inst: Share) = {
    PriceDAO.removeFromWatchList(inst)
  }
}