/*
 @author Peter Johansson / Ironic programmer 
 */

package org.ironic.sbors.util
import org.ironic.sbors.model._
//import org.ironic.sbors.util.Logging

object FileHandler{
  def apply() = { new FileHandler() }
}

class FileHandler { //extends Logging {
  import java.io.{File,BufferedWriter,FileWriter,IOException}
  import scala.collection.mutable.{ HashMap => MutableHashMap}
  import scala.collection.immutable.{ HashMap }
  import scala.io.Source
  
  val rootDir = { scala.util.Properties.userDir }
  val filesDir = { rootDir + "/files"}
  val logFile = { rootDir + "/" + "sBors.log"}
  val pricelistsFile = { rootDir + "/conf/pricelists.txt"}
      
  def checkDir(dirName: String) = {
     val f = new File(dirName)
     if(!f.exists){
       f.mkdirs
       println("!! Created dir at : " + dirName)
     }
  }
  
  def fileExists(fileName: String) = {
    val f = new File(fileName)
    f.exists
  }
  
  def writeToFile(fileName: String, append: Boolean, rows: List[String]){
    // Check that dir exista or creat if not
    checkDir(filesDir)
    
    val bw = new BufferedWriter(new FileWriter(fileName,append))
    
    rows foreach { row =>
      bw.write(row + "\n")
    }
    
    bw.close()
  }
  
  def priceFile(ticker: String) = {
    rootDir + "/files/" + ticker.replace(" ","_") + ".txt"
  }
  
  def loadFile(fileName: String)  = {
    val fc = Source.fromFile(fileName)
    fc.getLines.toList
  }
  
}