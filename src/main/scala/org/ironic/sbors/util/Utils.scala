package org.ironic.sbors.util

case object Stop
case class Daily(date: String)

case class Bollinger(middle: Double, upper: Double, lower: Double)

object LogLevel extends Enumeration {
  val Error   = Value(" ERROR   ")
  val Warning = Value(" WARNING ")                                                                                                      
  val Info    = Value(" INFO    ")
  val Debug   = Value(" DEBUG   ")
}

//trait Logging{
//  import scala.actors.Actor
//  import scala.actors.Actor._
//  protected val dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss ")
//  protected def tstamp = { dateFormat.format(new java.util.Date()) }
//  
//  class Logger(clz: Any){
//    def log(msg: String, level: LogLevel.Value){
//      writer ! (tstamp + level + "[" +clz.getClass().getName() + "] " + msg)
//    }
//  }
//
//  protected val writer = actor {
//    loop {
//      react {
//        case s:String => println(s)
//        case Stop => 
//          println(tstamp + "... Stopping logger....")
//          exit()
//        case _ =>
//      }
//    }
//  }
//  
//  
//  object Logger {
//    def apply(clz: Any) = { new Logger(clz) }
//  }
//
//  def info(msg: String){ logit(msg, LogLevel.Info) }
//  def warn(msg: String){ logit(msg, LogLevel.Warning) }
//  def error(msg: String){ logit(msg, LogLevel.Error) }
//  def debug(msg: String){ logit(msg, LogLevel.Debug) }
//  protected def logit(msg:String, level: LogLevel.Value) = {
//    Logger(this).log(msg, level)
//  }
//  
//  def stopLogger = {
//    writer ! Stop
//  }
//}

//import annotation.tailrec
import org.ironic.sbors.model.Price

trait Util {
  def dmin(a: Double, b:Double): Double = { if(a>b) b else a }
  def dmax(a: Double, b:Double): Double = { if(a<b) b else a }
  
  def zeroPad(list: List[Double], size: Integer) : List[Double] = {
    ((0 until size).toList map {_ => 0d}) ::: list
  }
}



object PriceUtil extends Util{
  
  def max(data: List[Price]) : Double = {
    val res = data.map{p => p.high}.reduceLeft((a, b) => (if (a > b) a else b))
    res
  }
  
  def min(data: List[Price]) : Double = {
    val res = data.map{p => p.low}.reduceLeft((a, b) => (if (a < b) a else b))
    res
  }
  
  def slidingAvgOld(list: List[Double], interval: Int) :List[Double] = {
    val slides = for{i <- 0 until (list.size - interval)
    } yield avg(list.slice(i,i+interval))
    
    // fill first 'days' list with 0's'
    ((0 until interval).toList map {_ =>0d}) ::: slides.toList
  }
  
  def slidingAvg(list: List[Double], interval: Int) :List[Double] = {
    val slides = list.iterator.sliding(interval).toList
    val means = for{slide <- slides} yield avg(slide.toList)
    zeroPad(means, interval-1)
  }
  
  def avg(list: List[Double]) :Double = {
    list.foldLeft(0d){(a,b) => {a+b}} / list.size
  }
  
  /**
   * %K = (Current Close - Lowest Low)/(Highest High - Lowest Low) * 100
   * %D = 3-day SMA of %K
   */
  def stochastics(list: List[Price], interval: Int) :List[Double] = {
    // step forward in inteval
    def sto(list: List[Price]) = {
      val iMin = list.foldLeft(list(0).low){(d,p) => dmin(d,p.low)}
      val iMax = list.foldLeft(0d){(d,p) => dmax(d,p.high)}
      (list.last.last - iMin)/(iMax - iMin) * 100
    }
    
    val stoList = for{i <- 0 to (list.size - interval)
    } yield sto(list.slice(i,i+interval))
    
//    ((0 until interval).toList map {_ =>0d}) ::: stoList.toList
    zeroPad(stoList.toList, interval-1)
  }
  
  def sum(data: List[Double]) : Double = data.foldLeft(0d)(_+_)
  def isGain(a: Double, b:Double) : Boolean = a>b
  def isLoss(a: Double, b:Double) : Boolean = a<b
  
  def sumFiltered(l: List[Double], p: (Double, Double) => Boolean) : Double = {
    val filtered = for(i <- 1 until l.size; if(p(l(i), l(i-1)))) yield scala.math.abs(l(i)-l(i-1))
    sum(filtered.toList)
  }
  
  /**
   * RSI = 100 - 100/(1+RS)
   * RS = Average Gain / Average Loss
   * The very first calculations for average gain and average loss are simple 14 period averages.
   *    First Average Gain = Sum of Gains over the past 14 periods / 14.
   *    First Average Loss = Sum of Losses over the past 14 periods / 14
   * The second, and subsequent, calculations are based on the prior averages and the current gain loss:
   *    Average Gain = [(previous Average Gain) x 13 + current Gain] / 14.
   *    Average Loss = [(previous Average Loss) x 13 + current Loss] / 14.
   */
  def RSI(data: List[Double], iv: Int) : List[Double] = {
    def RS(a: Double, b: Double) : Double = {
      a/b
    }
    def rsi(avGain: Double, avLoss: Double) : Double = {
      if(avLoss == 0) 100
      else (100 - (100 / (1+RS(avGain, avLoss))))
    }
    
    def makeRSIList(pAG: Double, pAL: Double, prev: Double, rest: List[Double]): List[Double] = {
      if(rest == Nil){
        List[Double]()
      } else {
        val cur = rest.head
        val gain = if(isGain(cur, prev)) scala.math.abs(cur-prev) else 0d; 
        val loss = if(isLoss(cur, prev)) scala.math.abs(cur-prev) else 0d; 
        val cAG = (pAG * (iv-1) + gain) / iv.toDouble
        val cAL = (pAL * (iv-1) + loss) / iv.toDouble
        List[Double](rsi(cAG,cAL)) ::: makeRSIList(cAG, cAL, cur, rest.tail)
      }
    }
    
    if(data.size < iv) {
      return List[Double]()
    }
    // first possible is at period. that is if iv = 14 the first would be avg gain for 0-13
    val first = data.slice(0, iv+1)
    val firstAvGain = sumFiltered(first, isGain) / iv.toDouble
    val firstAvLoss = sumFiltered(first, isLoss) / iv.toDouble
    val firstRSI = rsi(firstAvGain, firstAvLoss)
    val frs = RS(firstAvGain, firstAvLoss)
    val rest = data.slice(iv+1, data.size)
    
    val rsis = makeRSIList(firstAvGain, firstAvLoss, data(iv), rest)
    
    zeroPad(List[Double](firstRSI) ::: rsis, iv)
  }
  
  /**
   * ROC = Rate of Change
   */
  def roc(list: List[Double], iv: Int) : List[Double] = {
    val rocList = for{ 
      i <- iv until list.size 
    } yield perc(list(i) , list(i-iv))
    zeroPad(rocList.toList, iv)
  }
  
  def perc(a: Double, b: Double) : Double = {
    return ((a - b) / b) * 100 
  }
  
  def wma(xs: List[Double], iv: Integer) : List[Double] = {
    def weightedSum(xs: List[Double], n: Integer) : Double = {
      xs match {
        case head :: tail => n*head + weightedSum(tail, tail.size)
        case Nil => 0
      }
    }
    def sumSer(n: Integer) : Integer = {
      (n * (n+1)) / 2
    }
    
    val slides = xs.iterator.sliding(iv).toList
    val wavgs = for {slice <- slides} yield weightedSum(slice.toList.reverse, slice.size) / sumSer(slice.size)
    
    zeroPad(wavgs, iv-1)
  }
  
  def closesList(data: List[Price]) : List[Double] = {
    (for{p <- data} yield p.last).toList
  }
  
  /*
   * Chaiking Money Flow
   *  1. Money Flow Multiplier = [(Close  -  Low) - (High - Close)] /(High - Low) 
   *  2. Money Flow Volume = Money Flow Multiplier x Volume for the Period
   *  3. 20-period CMF = 20-period Sum of Money Flow Volume / 20 period Sum of Volume 
   */
  def chaikingMF(data: List[Price], iv: Int) : List[Double] = {
    def mfMultiplier(p: Price) : Double = if((p.high - p.low)== 0) 0 else ((p.last - p.low) - (p.high - p.last)) / (p.high - p.low)
    def cmf(slice: List[Price]) : Double = {
      val vols =slice.map {p => (p.returnNo, p.returnNo*mfMultiplier(p)) }
      val sum1 = sum(vols.map{v => v._1})
      val sum2 = sum(vols.map{v => v._2})
      if(sum1== 0) 0 else sum2 / sum1
    }
    
    val slides = data.sliding(iv, 1)
    val cmfs = for(slide <- slides) yield cmf(slide)
    
    zeroPad(cmfs.toList, iv-1)
  }
}

object CoppockUtil extends Util {
  def coppock(closes: List[Double], rPer1: Integer, rPer2: Integer, wmaPer: Integer) : List[Double] = {
    val roc1 = PriceUtil.roc(closes, rPer1)
    val roc2 = PriceUtil.roc(closes, rPer2)
    val roc1Plus2 = for{i <- 0 until closes.size} yield roc1(i)+roc2(i)
    // 0:s before "full" sets to calculate on
    def start = (wmaPer + (if(rPer1>rPer2) rPer1 else rPer2) -1)
    val data = roc1Plus2.toList.slice(23, roc1Plus2.size)
    zeroPad(PriceUtil.wma(data, wmaPer), start)
  }
}

object AroonUtil extends Util {
  def intervalSinceHighestHigh(inlist: List[Price]) : Int = {
    def ishh(list: List[Price], idx: Int, iv: Int, max: Double) : Int = {
      if(list == Nil){
        return iv
      } 
      val cur = list.head
      val m = if(cur.high > max) cur.high else max
      val i = if(cur.high > max) idx else iv
      ishh(list.tail, idx+1, i, m)
    }
    ishh(inlist.reverse, 0, 0, 0d)
  }
  
  def intervalSinceLowestLow(inlist: List[Price]): Int = {
    def isll(list: List[Price], idx: Int, iv: Int, min: Double) : Int = {
      if(list == Nil){
        return iv
      } 
      val cur = list.head
      val m = if(cur.low < min) cur.low else min
      val i = if(cur.low < min) idx else iv
      isll(list.tail, idx+1, i, m)
    }
    isll(inlist.reverse, 0, 0, 99999d)
  }
  
  def aroon(d: Int, iv: Int): Double = {
    val a = (iv - d)/iv.toDouble * 100
    //println("(" + iv +"-"+d+")/"+iv.toDouble+"* 100 => " + a)
    a
  }
  /**
   * Aroon-Up = ((25 - Days Since 25-day High)/25) x 100
   * Aroon-Down = ((25 - Days Since 25-day Low)/25) x 100
   */
  def aroonList(priceList: List[Price], interval: Int, up: Boolean) : List[Double] = {
    if(priceList.size <= interval){
      List[Double]()
    }
    val aroonList = for{i <- 0 to (priceList.size - interval)
    } yield if(up){
      aroon(intervalSinceHighestHigh(priceList.slice(i,i+interval)),interval) 
    }
    else { 
      aroon(intervalSinceLowestLow(priceList.slice(i,i+interval)),interval)
    }
                    
//    val retList = ((0 until interval).toList map {_ =>0d}) ::: aroonList.toList
//    retList
    zeroPad(aroonList.toList, interval-1)
  } 
}

object MAvgUtil extends Util {
  private def multiplier(iv: Int): Double = {
    (2 / (iv + 1).toDouble)
  }
  
  def avg(list: List[Double]) :Double = {
    list.foldLeft(0d){(a,b) => {a+b}} / list.size
  }
  
  def simpleMovingAvg(list: List[Double], interval: Int) :List[Double] = {
    val slides = list.iterator.sliding(interval).toList
    val means = for{slide <- slides} yield avg(slide.toList)
    zeroPad(means, interval-1)
  }
  
  /**
   * EMA: {Close - EMA(previous day)} x multiplier + EMA(previous day).
   */
  def expMovingAvg(closes: List[Double], interval: Int) :List[Double] = {
    def ema(prevEMA: Double, rest: List[Double]) : List[Double] = {
      if(rest == Nil){
        List[Double]()
      } else {
        val cur = rest.head
        val cEMA = (cur-prevEMA)*multiplier(interval)+prevEMA
        cEMA :: ema(cEMA, rest.tail)
      }
    }
    
    val initial = simpleMovingAvg(closes.slice(0,interval),interval)
    
    val expList = ema(initial.last,closes.slice(interval,closes.size))
    
    initial ::: expList.toList
  }
  
  
  /**
   * MACD Line: (12-day EMA - 26-day EMA) 
   * Signal Line: 9-day EMA of MACD Line
   * MACD Histogram: MACD Line - Signal Line
   */
  def macd(closes: List[Double], intervalA: Int, intervalB: Int) : List[Double] = {
    val expA = expMovingAvg(closes, intervalA)
    val expB = expMovingAvg(closes, intervalB)
    
    val exp = for{i <- 0 until expA.size} yield if(i<intervalB) 0d else expA(i) - expB(i)
    
    exp.toList
  }
  
  def macdHistogram(macdList: List[Double], signalList: List[Double]): List[Double] = {
    (for{i <- 0 until macdList.size} yield macdList(i) - signalList(i)).toList
  }
  
  def stdDev(list: List[Double]) = {
    val a = avg(list)
    val pows = list map{d => math.pow(d-a,2)}
    val sum = pows.foldLeft(0d){(d,v) => d+v}
    math.sqrt(sum/list.size)
  }
  
  def bollinger(closeList: List[Double], interval: Int) : List[Bollinger]= {
    val avgList = simpleMovingAvg(closeList,interval)
    val bList = for{i<- 0 until (closeList.size-interval)
                    val std = stdDev(closeList.slice(i,i+interval))
    } yield Bollinger(avgList(i+interval), avgList(i+interval)+std*2, avgList(i+interval)-std*2)
    
    val startList = (for{i<-0 until interval} yield Bollinger(0d,0d,0d)).toList
    startList.toList ::: bList.toList
  }
}

object ParabolicsUtil extends Util {
  /*
   * Underlaying parabolic arc of a trend
   * uses the most recent extreme low/high price  (EP)
   * with an accelerator factor (AF)
   * 
   * EP = highest hig for a long trend and lowst low for a short trend, updated each time a new EP is found
   * AF = Default 0.02 increasing by 0.02 each time a new EP is found with maximum 0.2
   * 
   * SAR(i) = SAR(i-1)+ACCELERATION*(EPRICE(i-1)-SAR(i-1))
   */
  
  def parabolics(list: List[Price]) : List[Double]= {
    // check first 2
    // when SAR is within range, reverse
    // Assume long position, uptrend sip is prior low ep (downtrend sip = prior high ep)
    // reversals should start at highest/lowest point since last swap
    var pSar = 0d
    var af=0.02
    var ep = 0d
    // check at pos 1
    def upSar = { pSar + (af * (ep  - pSar)) }
    def downSar = { pSar - (af * (pSar - ep)) }
    var pbList = List(0d)
    var isUp = true
    pSar = list(0).low
    var aep = pSar
    var swapped = false
      
    for {i <- 1 until list.size} {
      if(swapped){
        pSar = aep
      }
      val cSar = if(isUp){ upSar } else { downSar }
      swapped = false
      //println("aep " + aep + " cSar " + cSar + " high " + list(i).high + " low " + list(i).low)
      // change direction if in "scope"
      if((isUp && cSar > list(i).low) || (!isUp && cSar < list(i).high)){
        isUp = !isUp
        //println("swap to " + (if(isUp){"up"}else{"down"}))
        af = 0d
        ep = if(isUp){list(i).low} else {list(i).high}
        //aep = if(isUp){list(i).high} else {list(i).low}
        swapped = true
      }
      
      if(isUp){
        if(list(i).high > ep){
          ep = list(i).high
          af += 0.02
          if(af > 0.2) af = 0.2
        }
        if(!swapped && list(i).high > aep){
          aep = list(i).high
        }
      } else {
        if(list(i).low < ep){
          ep = list(i).low
          af += 0.02
          if(af > 0.2) af = 0.2
        }
        if(!swapped && list(i).low < aep){
          aep = list(i).low
        }
      }
      
      pSar = if(swapped) {aep}  else {cSar}
      
      pbList = pbList ::: List(cSar)
    } 
    List(0d) ::: pbList
  }
}

object VolatileUtil extends Util {
  // The constant 'C' used in ARC, ARC = ATR * C
  val C = 3d
  def TR(yClose: Double, tHigh: Double, tLow: Double): Double = {
    val tr = math.max(math.abs(tHigh-tLow),math.max(math.abs(tHigh-yClose),math.abs(yClose-tLow)))
//    println("tr: " + tr)
    tr
  }
  def ATR_latest(period: Int,tr: Double, atr_prev:Double) = {
//    println("ATR_latest => (("+period+"-1) * "+atr_prev+" + "+tr+") / " + period)
    ((period-1) * atr_prev + tr) / period
  }
  
  def SIC(closes: List[Double],high: Boolean):Double = {
//    println("SIC list size : " + closes.size)
    val sic = if(high){
      closes.foldLeft(0d){(a,b) => a max b}
    } else {
      closes.foldLeft(Double.MaxValue){(a,b) => a min b}
    }
//    println(sic)
    sic
  }
  
  def ATRList(list: List[Price], period: Int) : List[Double] = {
    // calculate ATR for first 'peroid' days
    val ATR_init = MAvgUtil.avg((for{i <- 1 to period} yield TR(list(i-1).last,list(i).high,list(i).low)).toList)
    //println("atr_init"+ATR_init)
    var ATR_previous = ATR_init
    var rest = List[Double]()
    for(i <- (period) until list.size-1){
      val tr = TR(list(i-1).last,list(i).high,list(i).low)
      //println("tr "+tr)
      val atr = ATR_latest(period,tr,ATR_previous)
      ATR_previous = atr
      //println("atr_prev " + ATR_previous)
      rest = rest ::: List(atr)
    } 
    //println(rest)
    val start = for{i <- 0 until period} yield 0d
    start.toList ::: List(ATR_init) ::: rest.toList
  }
  
  def SARList(list: List[Price]) : List[Double] = {
    val period = 7
    val atrList = ATRList(list,period)
    val arcList = atrList map {d => d*C }
    var isUpTrend = true
    
    // check wether SIC_low ot SIC_high is closest to arc
    val sicHigh = VolatileUtil.SIC(list.slice(0,period).map{p=> p.last},true)
    val sicLow = VolatileUtil.SIC(list.slice(0,period).map{p=> p.last},false)
    
    val longSar = sicHigh - arcList(period)
    val shortSar = sicLow + arcList(period)
//    println("last => "+ list(period).last +", longSar => " + longSar + ", shortSar => " + shortSar)
    
    isUpTrend = (math.abs(list(period).last - longSar) >= math.abs(list(period).last - shortSar))
//    println("(" + math.abs(list(period).last - longSar) + " >= " + math.abs(list(7).last - shortSar)+")")
//    println("uptrend = " + isUpTrend)
    val firstSar = if(isUpTrend) {longSar} else {shortSar}
    var sarList = (for{i <- 0 until period } yield 0d).toList ::: List(firstSar)
    
    for(i <- (period+1) until list.size){
      //println("i="+i)
      val sic = VolatileUtil.SIC(list.slice((i-period-1), i-1).map{p=> p.last},isUpTrend)
      
      val sar = if(isUpTrend){
        val uSar = sic - arcList(i-1)
        uSar
      } else {
        val dSar = sic + arcList(i-1)
        dSar
      }
      isUpTrend = (sar < list(i).last)
      
//      println("ATR_list  : " + atrList)
//      println("idx:"+i+", sic:" +sic+", sar:"+sar+", arc(-1):"+arcList(i-1)+", atr(-1):" + atrList(i-1))
      sarList = sarList ::: List(sar)
    }
    
    sarList
  }
}