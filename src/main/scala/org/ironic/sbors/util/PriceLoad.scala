package org.ironic.sbors.util
import org.ironic.sbors.model._

class PriceLoad(sharesList: String) {
  def importShares() = {
    val shares = PriceDAO.loadShares(sharesList)
//    val shares = listToShares(list)
    shares foreach { share =>
      new YahooImporter(share).run()
    }
  } 
  
}

class YahooImporter(share: Share) {
  import scala.io.Source
  val baseUrl = "http://ichart.finance.yahoo.com/table.csv"
  val todayParams = {
    import java.util.{Calendar, GregorianCalendar}
    val cal = new GregorianCalendar()
    //cal.add(Calendar.DATE, -1)
    val d = cal.get(Calendar.DATE)
    val m = cal.get(Calendar.MONTH)
    val y = cal.get(Calendar.YEAR)
    "&d=" + m + "&e=" + d + "&f=" + y
  }
  def params(ticker: String) = {
    val yName = (ticker.replace(" ", "-")).toUpperCase
    "?s="+ yName + todayParams + "&g=d&a=0&b=3&c=2000&ignore=.csv"//"&d=9&e=5&f=2011&g=d&a=0&b=3&c=2000&ignore=.csv"
  }
  
  def run() {
    println("Fetching prices for " + share)
    val uri = baseUrl + params(share.yhName)
    
    try{
      val lines = readUrl(uri)
      println("Data length: " +lines.size)
      val handledLines = processLines(lines,share.ticker)
      println("Did pricelines lines : " + handledLines.size)
      PriceDAO.savePriceListToFile(handledLines, share.ticker)
    } catch {
      case x: Throwable => println(x.toString)
    }
  }
  
  def readUrl(url: String) : List[String] = {
    val fc = Source.fromURL(url).getLines().toList 
    fc
  }
  
  def processLines(lines: List[String], ticker: String) : List[Price] = {
    val YHPriceExtractorRE = """([[^Date]^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+).*""".r
    val handled = lines.reverse.map { line =>  
      line match { 
        case YHPriceExtractorRE(date,open,high,low,close,vol,ac)
          => val p = Price(date.replace("-",""),ticker,ticker,0d,0d,0d,0d,open.toDouble,close.toDouble,high.toDouble,low.toDouble,vol.toDouble,0d) 
          p
        case _ => println("Found line : " + line)
      } 
    }
    val x = for{l <- handled 
                if(l.isInstanceOf[Price])
                  } yield l.asInstanceOf[Price]
                x
    }
  }

  object PriceLoad {
    def apply(sharesList: String) = new PriceLoad(sharesList)
  }
