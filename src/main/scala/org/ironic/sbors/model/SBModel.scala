package org.ironic.sbors.model

// To be able to handle Iterator[PriceItem]
trait PriceItem 

case class InvalidPrice(date: String, content:String) extends PriceItem
case class PriceList(name:String) extends PriceItem
case class Price(date: String, ticker :String, aktie : String,
    diffKr : Double, diffPerc : Double, buy : Double,
    sell : Double, open: Double, last : Double, high : Double,
    low : Double, returnNo : Double, returnKr : Double
  ) extends PriceItem {
    def this(date:String, open: Double, high: Double, low: Double, close: Double) = this(date,"","",0d,0d,0d,0d,open,close,high,low,0d,0d)
  }
case class Share(ticker:String, name: String,yhName: String)  
case class ShareList(id:String, name: String, instruments: List[Share])
