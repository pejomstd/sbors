/*
 @author Peter Johansson / Ironic programmer 
 */
package org.ironic.sbors.model

/**
 * Provides service methods and communicate with underlaying storage
 */
//import me.ironic.scabors.util.Logging
import org.ironic.sbors.model._
import org.ironic.sbors.util.FileHandler
import scala.collection.mutable.{ HashMap => MutableHashMap}
import scala.collection.immutable.{ HashMap }
  
object PriceDAO { // extends Logging{
  
  def saveDailyPrices(prices: List[PriceItem]) = {
    println("## Saving daily prices")
       
    var lists = new MutableHashMap[String,List[Price]]
    var curList = "?"
    
    prices foreach { price =>
      price match { 
        case q: Price => { 
                            savePriceToFile(q);
                            lists(curList) = List(q) ::: lists(curList)
                          }
        case qList: PriceList => {
                                    curList = qList.name;
                                    lists += qList.name -> List()
                                 }
//        case _ => warn(price.toString)
        case _ => println(price.toString)
      }
    }
    
    // Save current lists to file...
    saveStockPriceList(lists)
  }
  
  private def fetchLastDate(inst: Share) :String = {
    val data = loadShareData(inst)
//    info("Last price day : " + data.last.date)
    println("Last price day : " + data.last.date)
    data.last.date
  }
  
  
  /**
   * Method for saving a price to file
   */
  protected def savePriceToFile(price: Price) = {
    // send dummy Share with ticker for load
    val lastDate = try { 
      fetchLastDate(Share(price.ticker,price.ticker,""))
    } catch {
//      case err => warn("Error in savePriceToFile trying to fetch last date : " + err)
      case err: Throwable => println("Error in savePriceToFile trying to fetch last date : " + err)
        "19700101" // return a very low date
    }
    println("price.date.toInt > lastDate.toInt" + price.date.toInt  + " > " + lastDate.toInt)
    if(price.date.toInt > lastDate.toInt){
      val fileHandler = FileHandler()
      val fName = fileHandler.priceFile(price.ticker)
      fileHandler.writeToFile(fName, true, List(price.toString))
//      info("Saved price for : " + price.date + ", " + price.ticker)
      println("Saved price for : " + price.date + ", " + price.ticker)
    } else {
//      info("File have same or later day, will not save this : " + price)
      println("File have same or later day, will not save this : " + price)
    }
  }
  
  def savePriceListToFile(prices: List[Price], ticker: String) = {
    val fileHandler = FileHandler()
    val fName = fileHandler.priceFile(ticker)
    fileHandler.writeToFile(fName, false, {prices map { p => p.toString}}.toList)
  }
  
  /**
   * Method for saving an updated pricelist map to file.
   */
  protected def saveStockPriceList(pricelist: MutableHashMap[String,List[Price]]){
//    info("## Saving pricelists")
    println("## Saving pricelists")
    
    val sb = new StringBuffer
    sb.append("{ \"pricelists\": [\n")
    for{ (listname,prices) <- pricelist } {
      sb.append("\t{ \n\t\t\"listname\": \"" + listname + "\",\n")
      sb.append("\t\t\"stocks\": [\n")
      for{ price <- prices } {
        sb.append("\t\t\t{ \"ticker\": \""+price.ticker + "\", \"aktie\": \"" + price.aktie + "\"},\n")
      }
      sb.append("\t\t],\n")
      sb.append("\t},\n")
    }
    sb.append("}\n")
    
    val fileHandler = FileHandler()
    val fName = fileHandler.pricelistsFile
    fileHandler.writeToFile(fName, false, List(sb.toString))
  }
  
  /**
   * Method for loading price lists to a HashMap, 
   * each entry in the map is a list of Shares
   */
  def loadPriceLists  : MutableHashMap[String, List[Share]] = {
//    info("## Loading pricelists")
    println("## Loading pricelists")
    var pricelists = new MutableHashMap[String, List[Share]]
    val ListExtractorRE = """.*"listname": "(.+)",.*\s*""".r
    val InstExtractorRE = """.*"ticker": "(.+)", "aktie": "(.+)".*\s*""".r
   
    /*
     * When hitting a PriceList, a new list begins
     */
    var curList = "?"
    val fileHandler = FileHandler()
    val fName = fileHandler.pricelistsFile
    val rows = fileHandler.loadFile(fName)
    rows foreach { line =>
      line match {
        case ListExtractorRE(listname) => val pList = new PriceList(listname)
          curList = pList.name
          pricelists += pList.name -> List()
        case InstExtractorRE(ticker, aktie) => val inst = new Share(ticker, aktie,"") 
          pricelists(curList) = List(inst) ::: pricelists(curList)
        case _ => None
      }
      
    } 
    pricelists
  }
  
  /**
   * New structure for pricelists/instruments
   */
  def loadShareLists = {
//    info("## Loading instrument lists")
    println("## Loading instrument lists")
    val InstListExtractor = """ShareList\(([^\,]*),([^\,]*)\)""".r
    val fileHandler = FileHandler()
    val rows = fileHandler.loadFile("conf/instlists.scb")
    val instLists = rows filter{ row => row.startsWith("ShareList")} map {
      r => r match {
        case InstListExtractor(id,name) => ShareList(id, name,loadShares(id))
      }
    }
    instLists
  }
  
  def loadShares(id: String) : List[Share] = {
    val ShareExtractor = """Share\(([^\,]*),([^\,]*),([^\,]*)\)""".r
    val fileHandler = FileHandler()
    val rows = fileHandler.loadFile("conf/" + id)
    val instList = rows filter{ row => row.startsWith("Share")} map {
      r => r match {
        case ShareExtractor(ticker, name, yhName) => Share(ticker, name, yhName)
      }
    }
    
    instList
  }
  
  /**
   * Method for loading all prices for a specific instrument.
   */
  def loadShareData(inst: Share) : List[Price] = {
    val fileHandler = FileHandler()
//    info("Loading instrument " + inst)
    println("Loading instrument " + inst)
    val fName = fileHandler.priceFile(inst.ticker)
    
    val PriceExtractorRE = """Price\(([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*),([^\,]*).*\)""".r
    val pricelines = fileHandler.loadFile(fName)
    val prices = pricelines map { line =>
      line match {
        case PriceExtractorRE(date,ticker,aktie,diffKr,diffPerc,buy,sell,open,last,high,low,returnNo,returnKr) => 
          Price(date,ticker,aktie,diffKr.toDouble,diffPerc.toDouble,buy.toDouble,sell.toDouble,open.toDouble,last.toDouble,high.toDouble,low.toDouble,returnNo.toDouble,returnKr.toDouble)
        //case _ => //InvalidPrice("?", line)
      } 
    } 
    prices.toList
  }
  
  def addToWatchList(inst: Share) = {
//    info("PriceDAO.addToWatchList(" + inst + ")")
    println("PriceDAO.addToWatchList(" + inst + ")")
    val fileHandler = FileHandler()
    val instList = if(fileHandler.fileExists("conf/watchlist.scb")){
      inst :: loadShares("watchlist.scb")
    } else {
      List[Share](inst)
    }
//    info("instList " +instList)
    println("instList " +instList)
    fileHandler.writeToFile("conf/watchlist.scb", false, instList.map {i => i.toString})
  }
  
  
  def getWatchList() : List[Share] = {
//    info("PriceDAO.getWatchList()")
    println("PriceDAO.getWatchList()")
    val fileHandler = FileHandler()
    val instList = if(fileHandler.fileExists("conf/watchlist.scb")){
      loadShares("watchlist.scb")
    } else {
      List[Share]()
    }
    instList sortWith {(a,b) => a.name < b.name}
  }
  
  def removeFromWatchList(inst: Share) = {
//    info("Removing from watch list : " + inst)
    println("Removing from watch list : " + inst)
    val curList = getWatchList()
    
    // check if there is a better way
    val remList = curList.filter { i=> i.ticker != inst.ticker }
    val fileHandler = FileHandler()
    fileHandler.writeToFile("conf/watchlist.scb", false, remList.map {i => i.toString})
  }
}
