package org.ironic.sbors.view

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.{Insets, Pos, Orientation}
import scalafx.scene.control.Label
import scalafx.scene.layout.BorderPane
import scalafx.scene.layout.GridPane;
import scalafx.scene.control._
import javafx.{ application => jfxa }


import scalafx.event.ActionEvent
import javafx.beans.value.{ChangeListener,ObservableValue}
import javafx.scene.control.SeparatorMenuItem
import scalafx.scene.layout._
import scalafx.scene.{Scene, Group, Node}
import scalafx.stage.Stage
import scalafx.scene.chart.{LineChart, NumberAxis, XYChart, CategoryAxis}
import scalafx.collections.ObservableBuffer

import org.ironic.sbors.service.PriceService
import org.ironic.sbors.view._
import org.ironic.sbors.model._
import org.ironic.sbors.util._
 
object ScalaBorsApp extends JFXApp {
  var currentShare = Share("---", "Not choosen", "---")
  var curPriceList: List[Price] = Nil
  
  
  
  val sbMenu = new Menu("Application") {
    val exit = new MenuItem("Exit") { 
      onAction = { ae : ActionEvent =>
        stage.close()
      }
    }
    val settings = new MenuItem("Settings...") {
      onAction = { ae : ActionEvent =>
        SettingsStage().show()
      }
    }
    val priceLoad = new MenuItem("Load prices...") {
      onAction = { ae : ActionEvent =>
        PriceLoadStage().show()
      }
    }
    
    items.add(priceLoad)
    items.add(settings)
    items.add(new SeparatorMenuItem())
    items.add(exit)
  }
  val chartMenu = new Menu("Charts") {
    val aroon = new MenuItem("Aroon") {
      onAction = { ae : ActionEvent =>
        AroonStage().show()
      }
    }
    val candle = new MenuItem("Candle Stick") {
      onAction = { ae : ActionEvent =>
        CandleStickStage().show()
      }
    }
    val chaikin = new MenuItem("Chaikin Monet Flow") {
      onAction = { ae : ActionEvent =>
        ChaikinMFStage().show()
      }
    }
    val coppock = new MenuItem("Coppock") {
      onAction = { ae : ActionEvent =>
        CoppockStage().show()        
      }
    }
    val macd = new MenuItem("MACD") {
      onAction = { ae : ActionEvent =>
        MACDStage().show()
      }
    }
    val roc = new MenuItem("RoC") {
      onAction = { ae : ActionEvent =>
        RoCStage().show()
      }
    }
    val rsi = new MenuItem("RSI") {
      onAction = { ae : ActionEvent =>
        RSIStage().show()
      }
    }
    val stoch = new MenuItem("Stochastics") {
      onAction = { ae : ActionEvent =>
        StochasticsStage().show()
      }
    }
    val volume = new MenuItem("Volume") {
      onAction = { ae : ActionEvent =>
        VolumeStage().show()
      }
    }
    
    items.add(aroon)
    items.add(candle)
    items.add(chaikin)
    items.add(coppock)
    items.add(macd)
    items.add(roc)
    items.add(rsi)
    items.add(stoch)
    items.add(volume)
  }
  val listsMenu = new Menu("Lists") {
    val addW = new MenuItem("Add to watch") {
      onAction = { ae : ActionEvent =>
        
      }
    }
    val viewW = new MenuItem("Watchlist") {
      onAction = { ae : ActionEvent =>
        
      }
    }
    val lists = new MenuItem("Lists...") {
      onAction = { ae : ActionEvent =>
        
      }
    }
    items.add(addW)
    items.add(viewW)
    items.add(lists)
    
  }
  
  val menuBar = new MenuBar {
    useSystemMenuBar = true
    menus.add(sbMenu)
    menus.add(chartMenu)
    menus.add(listsMenu)

  }
  
  val lblCurShare = new Label {
    text = "-- No instrument choosen --"
    style = "-fx-font-size: 15px; -fx-font-weight: bold; -fx-text-fill: #ffffff;"
  }
  
  val lblLastPrice = new Label {
    style = "-fx-font-size: 10px; -fx-font-weight: bold; -fx-text-fill: #ffffff;"
  }
    
  var barChartPane = new ScrollPane {
    content = updateBarChart
  }
  
  def updateWithShare(share: Share){
    println("Update with instrument : " + share)
    println("Listing starts from : " + Settings.chartFrom)
    lblCurShare.text = share.name
    currentShare = share
    curPriceList = PriceService.getShareData(currentShare).filter(p => p.date.substring(0,Settings.chartFrom.length).toInt >= Settings.chartFrom.toInt)

    barChartPane.setContent(updateBarChart) 
    val lastPrice = curPriceList(curPriceList.size-1)
    lblLastPrice.text = s"${lastPrice.date} [close:${lastPrice.last}, high:${lastPrice.high}, low:${lastPrice.low}]"
    printPrice(lastPrice)

  }
  
  def printPrice(p: Price) {
    println("Date: " + p.date)
    println("Last: " + p.last)
    println("Low: " + p.low)
    println("High: " + p.high)
  }
  
  stage = new PrimaryStage {
    title = "Scala-Bors"
    width = 1100
    height = 650
    scene = new Scene {
      root = new BorderPane {
        top = menuBar
        center = new BorderPane {
          top = createSearchPanel
          center = barChartPane
        }
      }
    }
  }
  
  private def createSearchPanel = {
    val stockTxt = new TextField {
      prefColumnCount = 10
      promptText = "a"
    }
    val instrCombo = new ComboBox[String](Seq("-- Search to populate --")) {
      value.addListener(new ChangeListener[String]() {
          @Override 
          def changed(ov: ObservableValue[_ <: String],t: String,t1: String) = {   
            if(t1 != null) {
              println("changed... " + t1)       
              val share = new Share(t1.split(":")(0),t1.split(":")(1).trim,"")
              ScalaBorsApp.updateWithShare(share)
            }
          }    
        });
    }
    val searchBtn = new Button {
      text = "Search"
      onAction = (ae : ActionEvent) => {
            val prevItems = for { i <- instrCombo.items.get } yield i
            prevItems.foreach(i => instrCombo -= i)
            val hits = PriceService.searchSharesByTicker(stockTxt.text.get) sortWith {(i1,i2) => i1.name.toLowerCase < i2.name.toLowerCase}
//          println("hits: " + hits)
            hits foreach { hit =>
              instrCombo += hit.ticker + ": " + hit.name
            }
          
        }
    }
    
    new HBox(6) {
      padding = Insets(20)
      content = Seq(
        new Label("Share"), 
        stockTxt, 
        searchBtn, 
        instrCombo,
        new Separator(){orientation = Orientation.VERTICAL},
        new CheckBox {
          text = "Show avg."
          onAction = { ae : ActionEvent =>
            showAvg(selected.value) 
          }
        },
        new CheckBox {
          text = "Bollinger bands"
          onAction = { ae : ActionEvent =>
            showBollinger(selected.value) 
          }
        },
        new CheckBox {
          text = "Parabolics"
          onAction = { ae : ActionEvent =>
            showParabolics(selected.value) 
          }
        },
//        new CheckBox {
//          text = "Volatile"
//          onAction = {
//            showVolatile(selected.value) 
//          }
//        },
        new Separator(){orientation = Orientation.VERTICAL},
        lblCurShare, 
        lblLastPrice
      )
      style = "-fx-background-color: #336699;"
    }
  }
  
  def showAvg(show: Boolean) = {
    if(show) {
      val avgs = Settings.movingAvgPeriods
      // show all that is not 0
      val avg1 = MAvgUtil.expMovingAvg({curPriceList map { p => p.last }}, avgs(0))
      val avg2 = MAvgUtil.expMovingAvg({curPriceList map { p => p.last }}, avgs(1))
      val avg3 = MAvgUtil.expMovingAvg({curPriceList map { p => p.last }}, avgs(2))
     
      bcSeries("avg1").data.get.clear
      bcSeries("avg2").data.get.clear
      bcSeries("avg3").data.get.clear
      
      for{ i <- 0 until curPriceList.size } {
        val date = curPriceList(i).date
        if(avg1(i) > 0) {
          bcSeries("avg1").data.get.add(XYChart.Data[String, Number](date, avg1(i)).delegate)
        }
        if(avg2(i) > 0) {
          bcSeries("avg2").data.get.add(XYChart.Data[String, Number](date, avg2(i)).delegate)
        }
        if(avg3(i) > 0) {
          bcSeries("avg3").data.get.add(XYChart.Data[String, Number](date, avg3(i)).delegate)
        }
      }
    } else {
      bcSeries("avg1").data.get.clear
      bcSeries("avg2").data.get.clear
      bcSeries("avg3").data.get.clear
    }
  }
  
  def showBollinger(show: Boolean) = {
    if(show) {
      bcSeries("bollUp").data.get.clear
      bcSeries("bollMid").data.get.clear
      bcSeries("bollLow").data.get.clear
      
      val bollingerList = MAvgUtil.bollinger(curPriceList.map{p=>p.last},20)
      for{ i <- 0 until curPriceList.size } {
        val date = curPriceList(i).date
        bcSeries("bollUp").data.get.add(XYChart.Data[String, Number](date, bollingerList(i).upper).delegate)
        bcSeries("bollMid").data.get.add(XYChart.Data[String, Number](date, bollingerList(i).middle).delegate)
        bcSeries("bollLow").data.get.add(XYChart.Data[String, Number](date, bollingerList(i).lower).delegate)
      }
    } else {
      bcSeries("bollUp").data.get.clear
      bcSeries("bollMid").data.get.clear
      bcSeries("bollLow").data.get.clear
      
    }
  }
  
  def showVolatile(show: Boolean) = {
    if(show) {
      bcSeries("volatile").data.get.clear
      def volatileList = VolatileUtil.SARList(curPriceList) 
      for{ i <- 0 until curPriceList.size } {
        val date = curPriceList(i).date
        bcSeries("volatile").data.get.add(XYChart.Data[String, Number](date, volatileList(i)).delegate)
      }
    } else {
      bcSeries("volatile").data.get.clear
    }
  }
  
  def showParabolics(show: Boolean) = {
    if(show) {
      bcSeries("parabolics").data.get.clear
      val parabolics = ParabolicsUtil.parabolics(curPriceList)
      for{ i <- 0 until curPriceList.size } {
        val date = curPriceList(i).date
        bcSeries("parabolics").data.get.add(XYChart.Data[String, Number](date, parabolics(i)).delegate)
      }
    } else {
      bcSeries("parabolics").data.get.clear
    }
  }
  
  def emptyData = ObservableBuffer(List[Double]()) map {t => XYChart.Data[String, Number]("", t).delegate}
  
  val bcSeries = collection.mutable.Map[String,XYChart.Series[String, Number]](
    "prices" -> XYChart.Series[String, Number]("Price", emptyData),
    "avg1" -> XYChart.Series[String, Number]("avg1", emptyData),
    "avg2" -> XYChart.Series[String, Number]("avg2", emptyData),
    "avg3" -> XYChart.Series[String, Number]("avg3", emptyData),
    "bollUp" -> XYChart.Series[String, Number]("bollUp", emptyData),
    "bollMid" -> XYChart.Series[String, Number]("bollMid", emptyData),
    "bollLow" -> XYChart.Series[String, Number]("bollLow", emptyData),
    "parabolics" -> XYChart.Series[String, Number]("parabolics", emptyData),
    "volatile" -> XYChart.Series[String, Number]("volatile", emptyData)
  )

  private def updateBarChart = {
    if(!curPriceList.isEmpty) {
      val min = curPriceList.map{p => p.last}.reduceLeft((a, b) => (if (a < b) a else b))
      val max = curPriceList.map{p => p.last}.reduceLeft((a, b) => (if (a > b) a else b))
    
      println("Max: " + max)
      println("Min: " + min)
      val xAxis = new CategoryAxis();
      val yAxis = NumberAxis(min, max, (max-min)*0.1);
      xAxis.setLabel("Date");
      val lineChart = LineChart(xAxis,yAxis) 
      lineChart.prefWidth = 2000
      lineChart.prefHeight = 500
      val data1 = ObservableBuffer(curPriceList) map {p => XYChart.Data[String, Number](p.date, p.last).delegate}
      val series1 = XYChart.Series[String, Number]("Price", data1)
      bcSeries += "prices" -> XYChart.Series[String, Number]("Price", data1)
    
      lineChart.getData().add(bcSeries.get("prices").get)
      lineChart.getData().add(bcSeries.get("avg1").get)
      lineChart.getData().add(bcSeries.get("avg2").get)
      lineChart.getData().add(bcSeries.get("avg3").get)
      lineChart.getData().add(bcSeries.get("bollUp").get)
      lineChart.getData().add(bcSeries.get("bollMid").get)
      lineChart.getData().add(bcSeries.get("bollLow").get)
      lineChart.getData().add(bcSeries.get("parabolics").get)
      lineChart.getData().add(bcSeries.get("volatile").get)
    
      lineChart.stylesheets += this.getClass.getResource("/BCStyle.css").toExternalForm
      new HBox(6) {
        content = Seq(lineChart)
      }
    } else {
      new HBox(6) {
        content = Seq(new Label("No choosen"))
      }
    }
  
  }
}

   