package org.ironic.sbors.util

import org.scalatest._

class MAvgUtilSuite extends FunSuite {
  def ema(c:Double, pEma: Double, mult: Double): Double = {
    (c-pEma)*mult + pEma
  }
  test("SMA should behave correctly") {
    val sliding = MAvgUtil.simpleMovingAvg(List[Double](1,2,2,4,4,6), 2)
    assert(sliding.size === 6)
    assert(sliding === List[Double](0,1.5,2,3,4,5))
    
    val sliding2 = MAvgUtil.simpleMovingAvg(List[Double](11,12,13,14,15,16,17), 5)
    assert(sliding2.size === 7)
    assert(sliding2 === List[Double](0,0,0,0,13,14,15))
  }
  
  test("EMA should behave correctly") {
    
    /* 50% multiplier = (2 / 0.5) - 1 = 3 periods */
    val sliding = MAvgUtil.expMovingAvg(List[Double](1,1,1,1,1,1), 3)
    assert(sliding.size === 6)
    assert(sliding === List[Double](0,0,1,1,1,1))
    
    /* 2 / (2 + 1) = 0.333 */
    val sliding2 = MAvgUtil.expMovingAvg(List[Double](10,10,10,11,12,12), 2)
    assert(sliding2.size === 6)
    assert(sliding2 === List[Double](0,10,ema(10,10,2d/3d),ema(11,ema(10,10,2d/3d),2d/3d), ema(12,ema(11,ema(10,10,2d/3d),2d/3d),2d/3d), ema(12, ema(12,ema(11,ema(10,10,2d/3d),2d/3d),2d/3d), 2d/3d)))
    
    val sliding3 = MAvgUtil.expMovingAvg(List[Double](10,10,10,11,12,12), 3)
    assert(sliding3.size === 6)
    assert(sliding3 === List[Double](0,0,10,ema(11,10,0.5),ema(12,ema(11,10,0.5),0.5),ema(12,ema(12,ema(11,10,0.5),0.5),0.5)))
  }
  
  test("MACD should be correct") {
    val list = List[Double](10,10,10,11,12,12)
    val macd = MAvgUtil.macd(list, 1, 3)
    assert(macd.size === 6)
    
    val ema1 = MAvgUtil.expMovingAvg(list, 1)
    val ema2 = MAvgUtil.expMovingAvg(list, 3)
    val ref = for{i <- 0 until ema1.size } yield if(i<3) 0d else ema1(i)-ema2(i)
    assert(macd === ref.toList)
  }
}
